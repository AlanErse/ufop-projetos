//Server

//#pragma comment(lib,"ws2_32.lib")
#include <WinSock2.h>
#include <iostream>


using namespace std;
int main() {

	//Winsock StartUp
	WSAData wsaData;
	WORD DllVersion = MAKEWORD(2, 1);

	if (WSAStartup(DllVersion, &wsaData) != 0) {

		MessageBoxA(NULL, "Winsock startup failed", "Error", MB_OK | MB_ICONERROR);
		exit(1);

	}

	SOCKADDR_IN addr; //Endereço de conexão do socket
	int addrlen = sizeof(addr); //Tamanho do endereço
	addr.sin_addr.s_addr = inet_addr("127.0.0.1"); //Broadcast local
	addr.sin_port = htons(4455); //Porta
	addr.sin_family = AF_INET; //IPv4 socket

	SOCKET sListen = socket(AF_INET, SOCK_STREAM, NULL); //Criação do socket para ouvir novas comunicações
	bind(sListen, (SOCKADDR*)&addr, sizeof(addr)); //Conecta o endereço ao socket
	listen(sListen, SOMAXCONN); //Posiciona o socket na posição de ouvir uma conecção nova

	SOCKET newConnection; //Socket para hospedar a conexão do cliente
	newConnection = accept(sListen, (SOCKADDR*)&addr, &addrlen); // Aceita nova conexão

	if (newConnection == 0) { cout << "Erro ao aceitar a conexão " << endl; }
	else { 
	
		cout << "Cliente conectado!" << endl; 
		char buffer[265] = "AE FUNCIONA!";
		send(newConnection, buffer, sizeof(buffer), NULL);//Envia a mensagem
	
	}
	system("pause");

	return 0;
}
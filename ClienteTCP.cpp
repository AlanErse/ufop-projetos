//Client

//#pragma comment(lib,"ws2_32.lib")
#include <WinSock2.h>
#include <iostream>
#include <errno.h>

using namespace std;

int main() {

	//Winsock StartUp
	// -lws2_32 tem que usar pra compilar
	WSAData wsaData;
	WORD DllVersion = MAKEWORD(2, 1);

	if (WSAStartup(DllVersion, &wsaData) != 0) {

		MessageBoxA(NULL, "Winsock startup failed", "Error", MB_OK | MB_ICONERROR);
		exit(1);

	}

	SOCKADDR_IN addr; //Endereço de conexão do socket
	int addrlen = sizeof(addr); //Tamanho do endereço
	addr.sin_addr.s_addr = inet_addr("127.0.0.1"); //Broadcast local, ip localhost
	addr.sin_port = htons(4455); //Porta
	addr.sin_family = AF_INET; //IPv4 socket

	SOCKET Connection = socket(AF_INET, SOCK_STREAM, NULL);; //Socket para a conexão do cliente
	int test;
	test = connect(Connection, (SOCKADDR*)&addr, sizeof addr);
	if (test != 0) { 
		
		MessageBoxA(NULL, "Falha ao conectar", "Erro", MB_OK | MB_ICONERROR);
		return 0;

	}

	cout << "Conectado!" << endl;
	char buffer[265];
	
	recv(Connection, buffer, sizeof(buffer), NULL);
	cout <<"Buffer :" << buffer << endl;
	
	system("pause");

	return 0;
}